from lib.solver import SolvableWorldRemote
from time import time
import argparse
import progressbar

__author__ = "Nelle Fuseau and Bertille Dubosc de Pesquidoux"
__copyright__ = "Copyright 2020, UTC"
__license__ = "WTFPL-2.0"
__version__ = "0.1.0"
__maintainer__ = "Nelle Fuseau and Bertille Dubosc de Pesquidoux"
__email__ = "bduboscd@etu.utc.fr"
__status__ = "dev"


class TimeForNextMaze(Exception):
    pass


if __name__ == "__main__":
    # Sommes-nous en mode verbose ? debug ?
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", help="Mode verbeux, donne des informations sur \
                        l'exploration", action="store_true")
    parser.add_argument("-d", help="Mode debug, donne des informations sur la \
                        communication avec le serveur", action="store_true")
    args = parser.parse_args()
    verbose = True if args.v else False
    log = True if args.d else False
    # Connexion au server
    server = "http://localhost:8080"
    groupe_id = "PRJ28"  # votre vrai numéro de groupe
    names = "Nelle Fuseau et Bertille Dubosc de Pesquidoux"  # vos blazes

    swr = SolvableWorldRemote(server, groupe_id, names, log, verbose)

    # Récupération du premier labyrinthe
    status, msg, size = swr.next_maze()
    while status == "[OK]":
        start = time()
        print("\n\n\n\n", msg)
        print(f"Taille du monde {swr.wwr.maze_number} : {size}\n\n")
        try:
            ###################
            ##### PHASE 1 #####
            ###################

            phase, pos = swr.wwr.get_status()
            print("Statut de la chasse :", phase, pos)
            gold = swr.wwr.get_gold_infos()
            print(f"Vous avez {gold} pièces d'or")

            if not verbose:
                bar = progressbar.ProgressBar(maxval=size*size,
                                              widgets=[progressbar.Bar('=', '[', ']'), ' ',
                                              progressbar.Percentage()])
                bar.start()

            # Tant qu'il nous manque des connaissances sur certaines cases
            while len(swr.get_uncertain_pos()) != 0:
                status, percepts, cost = swr.explore()
                if verbose:
                    print(status, percepts, cost)
                else:
                    bar.update(size*size - len(swr.get_uncertain_pos()))
                # On n'est jamais à l'abri d'une boulette
                if status == "[KO]":
                    raise TimeForNextMaze()

            if not verbose:
                bar.finish()

            phase, pos = swr.wwr.get_status()
            print("Statut de la chasse :", phase, pos)
            gold = swr.wwr.get_gold_infos()
            print(f"Vous avez {gold} pièces d'or")

            status, msg = swr.wwr.end_map()
            print(status, msg, "\n\n")

            middle = time()

            ###################
            ##### PHASE 2 #####
            ###################

            phase, pos = swr.wwr.get_status()
            print("Statut de la chasse :", phase, pos)

            # On met à jour la liste de cibles pour l'optimiser
            swr.order_targets()

            if not verbose:
                print("Marche jusqu'aux différentes positions :")
                bar.start()

            # Tant qu'on a des cibles à atteindre (l'or puis le point de
            # départ), on y va
            while len(swr.get_targets()) != 0:
                i, j = swr.wwr.get_position()
                status, msg, cost = swr.go_to_target(i, j)
                if verbose:
                    print(status, msg, cost)
                else:
                    bar.update(size*size - len(swr.get_targets()))

                # Pareil, on n'est pas à l'abri
                if status == "[KO]":
                    raise TimeForNextMaze()

            if not verbose:
                bar.finish()

            phase, pos = swr.wwr.get_status()
            print("Statut de la chasse :", phase, pos)
            gold = swr.wwr.get_gold_infos()
            print(f"Vous avez {gold} pièces d'or")

            res = swr.wwr.maze_completed()
            print(res, "\n\n")

            raise TimeForNextMaze()


        except TimeForNextMaze:
            end = time()
            print(f"La chasse a duré {end-start} secondes, dont \
{middle-start} secondes pour la phase 1 et {end-middle} pour la phase 2.")
            # print("Press enter for next maze\n\n\n\n")
            # input()
            status, msg, size = swr.next_maze()
            continue
