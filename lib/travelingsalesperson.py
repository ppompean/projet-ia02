from typing import List, Tuple
from lib.utils import man_distance, shift
from lib.pathfinder import astar
import progressbar


# Une classe pour éviter de tout recalculer
class PathOptimizer():
    # On garde en mémoire les positions sur lesquelles on peut passer et les
    # valeurs déjà calculées
    def __init__(self, pos_ok: List, targets: List, verbose=False):
        self.verbose = verbose
        self.pos_ok = pos_ok
        self.targets = targets
        self.step_costs = {}
        self.path_costs = {}
        self.current_path = []

    # Calcul du coût d'une étape d'un circuit, soit le chemin trouvé avec A*
    def step_cost(self, start: Tuple, end: Tuple) -> int:
        # Pour l'index, on trie le début et la fin
        if start > end:
            start, end = end, start
        if (start, end) not in self.step_costs:
            step = astar(self.pos_ok, start, end)
            self.step_costs[(start, end)] = step
            if step is None:
                self.step_costs[(start, end)] = man_distance(start, end)
            else:
                self.step_costs[(start, end)] = len(step)
        return self.step_costs[(start, end)]

    # Calcul du coût total d'un circuit, compris comme une liste de positions
    # à visiter + le retour à la case départ
    def path_cost(self, path: List) -> int:
        if (0, 0) in path:
            while path[-1] != (0, 0):
                shift(path)
        if tuple(path) not in self.path_costs:
            cost = sum(self.step_cost(path[i], path[i+1])
                       for i in range(len(path)-1))
            cost += self.step_cost(path[-1], path[0])
            self.path_costs[tuple(path)] = cost
        return self.path_costs[tuple(path)]

    # Une fonction qui donne un chemin avec les étapes classées gloutonnement,
    # pour une première approximation
    def glouton(self) -> List:
        print(f"Tri des cibles selon un ordre glouton :")
        # Pour ne pas endommager la liste de départ
        target_list = self.targets.copy()

        # Parce que le calcul est long
        bar = progressbar.ProgressBar(maxval=len(self.targets),
                                      widgets=[progressbar.Bar('=', '[', ']'), ' ',
                                      progressbar.Percentage()])
        bar.start()

        # On prend la première étape
        current_pos = target_list.pop()

        # La liste qui servira à la fin
        path = [current_pos]

        while len(target_list) != 0:
            bar.update(len(self.targets) - len(target_list))

            distance = {target: self.step_cost(current_pos, target)
                        for target in target_list}

            # On prend la cible la plus proche
            next_target = min(distance, key=distance.get)
            path.append(next_target)
            target_list.remove(next_target)

            # Prochaine itération
            current_pos = next_target
        bar.finish()

        self.current_path = path
        if self.verbose:
            print(f"Le chemin {path} sera une première approximation (coût \
{self.path_cost(path)}).")
        return self.current_path

    # Une fonction qui intervertit deux étapes d'un chemin donné
    def interversion(self, path: List, i: int, j: int) -> List:
        new_path = path.copy()
        new_path[i], new_path[j] = path[j], path[i]
        return new_path

    # Une fonction qui intervertit l'étape la plus intéressante
    def better_path(self) -> List:
        # Pour dénombrer les interversions possibles
        steps_number = len(self.current_path)
        # Dictionnaire par compréhension
        possible_paths = [self.interversion(self.current_path, i, j)
                          for i in range(steps_number)
                          for j in range(i + 1, steps_number)]
        costs = [self.path_cost(p) for p in possible_paths]
        # Le meilleur choix
        best_path = costs.index(min(costs))
        # On vérifie que c'est bien le meilleur
        if costs[best_path] < self.path_cost(self.current_path):
            self.current_path = possible_paths[best_path]
        return self.current_path

    # Une fonction qui intervertit jusqu'à un optimum
    def optimization(self, maxiter: int = 100) -> List:
        for i in range(maxiter):
            previous_cost = self.path_cost(self.current_path)
            self.better_path()
            current_cost = self.path_cost(self.current_path)
            if current_cost >= previous_cost:
                if self.verbose:
                    print(f"Pas de meilleur chemin possible. L'optimisation a \
pris {i+1} itérations.")
                break
            if self.verbose:
                print(f"Un meilleur circuit a été trouvé ! Passage à \
{self.current_path} (coût {current_cost}).")
        return self.current_path


# Assemblage
def gen_opt_path(pos_ok: List, target_list: List, verbose=False) -> List:
    optimizer = PathOptimizer(pos_ok, target_list, verbose)
    optimizer.glouton()
    return optimizer.optimization()
