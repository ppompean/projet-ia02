from lib.utils import neighbors
from typing import Tuple, List


def gen_clauses(world_size: int) -> Tuple[List, List]:
    # Création de toutes les variables qui vont servir, i.e. le Wampus, les
    # fosses, la brise et la puanteur pour chaque case
    vocab = [f"W{i}.{j}"
             for i in range(world_size) for j in range(world_size)] \
            + [f"P{i}.{j}"
               for i in range(world_size) for j in range(world_size)] \
            + [f"S{i}.{j}"
               for i in range(world_size) for j in range(world_size)] \
            + [f"B{i}.{j}"
               for i in range(world_size) for j in range(world_size)]

    # La case (0, 0 est sûre)
    clauses = ["-W0.0", "-P0.0"]

    # On passe en revue toutes les cases de la map pour générer les indices
    for i in range(world_size):
        for j in range(world_size):
            # Génération des cases qui sont autour de la case considérée
            vicinity = neighbors((i, j), world_size)

            # Ce sont autant d'emplacements possibles du Wumpus
            wumpus_possible_pos = [f"W{neighbor[0]}.{neighbor[1]}"
                                   for neighbor in vicinity]

            # Si ça pue, c'est que le Wumpus est sur une case autour
            clauses.append(f"-S{i}.{j} " + " ".join(wumpus_possible_pos))

            # Si ça ne pue pas, il n'y a pas de Wumpus
            for pos in wumpus_possible_pos:
                clauses.append(f"S{i}.{j} -{pos}")

            # Idem pour les fosses
            pit_possible_pos = [f"P{neighbor[0]}.{neighbor[1]}"
                                for neighbor in vicinity]

            # S'il y a de la brise, c'est qu'une fosse est sur une case autour
            clauses.append(f"-B{i}.{j} " + " ".join(pit_possible_pos))

            # S'il n'y a pas de brise, il n'y a pas de fosse
            for pos in pit_possible_pos:
                clauses.append(f"B{i}.{j} -{pos}")

    return(vocab, clauses)
