from lib.wumpus_client import WumpusWorldRemote
from lib.gopherpysat import Gophersat
from lib.pathfinder import astar
from lib.clauses import gen_clauses
from lib.travelingsalesperson import gen_opt_path
from lib.utils import shift, neighbors
from typing import Tuple, List
import pathlib

gophersat_exec = str(pathlib.Path(__file__).parent.absolute()) + "/gophersat"


# La classe pour un monde + un solveur, soit l'objet qui sera manipulé
# par le joueur
class SolvableWorldRemote:
    def __init__(self, server: str, groupe_id: str, names: str, log=False,
                 verbose=False):
        self.wwr = WumpusWorldRemote(server, groupe_id, names, log)
        self.verbose = verbose

    # Pour récupérer le prochain monde donné par le serveur
    def next_maze(self):
        status, msg, self.size = self.wwr.next_maze()

        vocab, clauses = gen_clauses(self.size)
        self.solver = Gophersat(gophersat_exec, vocab)
        for cl in clauses:
            self.solver.push_pretty_clause(cl.split(" "))

        # La liste des cases qu'on sait sûres
        self.__safe_pos = []
        # Celles pour lesquelles on ne sait pas encore
        self.__uncertain_pos = [(i, j)
                                for i in range(self.size)
                                for j in range(self.size)]
        # Celles qui sont dangereuses de façon certaine
        self.__dangerous_pos = []

        # Liste des positions pour envoyer des sondes
        self.__to_explore = [(0, 0)]

        # La liste des positions où il y a de l'or (et le (0, 0), où il faut
        # retourner)
        self.__targets = []

        return status, msg, self.size

    def get_uncertain_pos(self) -> List[Tuple]:
        return self.__uncertain_pos

    def get_safe_pos(self) -> List[Tuple]:
        return self.__safe_pos

    def get_dangerous_pos(self) -> List[Tuple]:
        return self.__dangerous_pos

    def get_to_explore(self) -> List[Tuple]:
        return self.__to_explore

    def get_targets(self) -> List[Tuple]:
        return self.__targets

    # Fonction qui dit si une position est ok, i.e. il n'y a ni Wumpus ni fosse
    def __is_safe(self, i: int, j: int) -> bool:
        # On ajoute la clauses pour pouvoir la tester
        # (au négatif parce que c'est la seule façon d'être sûr)
        clause = [f"W{i}.{j}", f"P{i}.{j}"]
        self.solver.push_pretty_clause(clause)
        result = self.solver.solve()
        # On l'enlève pour ne pas interférer avec les vraies informations
        self.solver.pop_clause()
        return not result

    # Est-ce que le Wumpus est là ?
    def __wumpus_there(self, i: int, j: int) -> bool:
        # On ajoute la clause pour pouvoir la tester
        # (au négatif parce que c'est la seule façon d'être sûr)
        clause = [f"-W{i}.{j}"]
        self.solver.push_pretty_clause(clause)
        result = self.solver.solve()
        # On l'enlève pour ne pas interférer avec les vraies informations
        self.solver.pop_clause()
        if not result:
            # Une nouvelle connaissance a été obtenue !
            clause = [f"W{i}.{j}"]
            self.solver.push_pretty_clause(clause)
            # Il faut aussi le dire au serveur
            self.wwr.know_wumpus(i, j)
            # Et au joueur
            if self.verbose:
                print(f"Selon toute logique, le Wumpus est en ({i}, {j}), pas \
besoin d'envoyer de sonde.")
            return True
        else:
            return False

    # De la même manière, est-ce qu'il y a une fosse ?
    def __pit_there(self, i: int, j: int) -> bool:
        # On ajoute la clause pour pouvoir la tester
        # (au négatif parce que c'est la seule façon d'être sûr)
        clause = [f"-P{i}.{j}"]
        self.solver.push_pretty_clause(clause)
        result = self.solver.solve()
        # On l'enlève pour ne pas interférer avec les vraies informations
        self.solver.pop_clause()
        if not result:
            # Une nouvelle connaissance a été obtenue !
            clause = [f"P{i}.{j}"]
            self.solver.push_pretty_clause(clause)
            # Il faut aussi le dire au serveur
            self.wwr.know_pit(i, j)
            # Et au joueur
            if self.verbose:
                print(f"Selon toute logique, il y a une fosse en ({i}, {j}), \
pas besoin d'envoyer de sonde.")
            return True
        else:
            return False

    def inspect_pos(self, i: int, j: int) -> str:
        # Si la position est déjà connue, ça ne sert à rien
        if (i, j) in self.get_safe_pos():
            return "safe"
        if (i, j) in self.get_dangerous_pos():
            return "dangerous"
        # Sinon, on l'inspecte
        if self.__is_safe(i, j):
            # On met la position dans la liste des endroits sûrs
            self.__safe_pos.append((i, j))
            self.__uncertain_pos.remove((i, j))
            # On retourne la valeur
            return "safe"
        if self.__pit_there(i, j) or self.__wumpus_there(i, j):
            # On met la position dans la liste des endroits dangereux
            self.__dangerous_pos.append((i, j))
            self.__uncertain_pos.remove((i, j))
            # On retourne la valeur
            return "dangerous"
        return "uncertain"

    # Fonction qui ajoute une connaissance à la base
    def __add_knowledge(self, i: int, j: int, percepts: str) -> None:
        # Si on sent de la puanteur, on peut ajouter cette clause
        if "S" in percepts:
            self.solver.push_pretty_clause([f"S{i}.{j}"])
        else:
            self.solver.push_pretty_clause([f"-S{i}.{j}"])
        # Idem pour la brise
        if "B" in percepts:
            self.solver.push_pretty_clause([f"B{i}.{j}"])
        else:
            self.solver.push_pretty_clause([f"-B{i}.{j}"])
        # Le Wumpus
        if "W" in percepts:
            self.solver.push_pretty_clause([f"W{i}.{j}"])
            # On le dit au serveur
            self.wwr.know_wumpus(i, j)
        else:
            self.solver.push_pretty_clause([f"-W{i}.{j}"])
        # Et les fosses
        if "P" in percepts:
            self.solver.push_pretty_clause([f"P{i}.{j}"])
            # On le dit au serveur
            self.wwr.know_pit(i, j)
        else:
            self.solver.push_pretty_clause([f"-P{i}.{j}"])
        # L'or n'a pas besoin d'être ajouté dans le solveur, mais il faudra
        # aller le chercher
        if "G" in percepts:
            self.__targets.append((i, j))
        # Avec ces nouvelles connaissances, la position a peut-être changé
        # de statut
        if (i, j) in self.get_uncertain_pos():
            if "P" in percepts or "W" in percepts:
                self.__dangerous_pos.append((i, j))
                self.__uncertain_pos.remove((i, j))
            else:
                self.__safe_pos.append((i, j))
                self.__uncertain_pos.remove((i, j))
        # On quitte la fonction
        return

    # Fonction qui retourne les voisins encore à explorer d'une position donnée
    def get_uncertain_neighbors(self, pos: Tuple) -> List[Tuple]:
        return list(set(neighbors(pos, self.size))
                    & set(self.get_uncertain_pos()))

    # Fonction d'exploration
    def explore(self) -> Tuple[str, str, str]:
        # Si on n'a rien sur le feu
        if len(self.get_to_explore()) == 0:
            # On prend la première position incertaine pour se débloquer
            (i, j) = self.get_uncertain_pos()[0]
            # On y envoie une sonde
            status, percepts, cost = self.explore_uncertain_pos(i, j)
            # Et on ajoute les voisins pas encore explorés à la liste de tâches
            vicinity = self.get_uncertain_neighbors((i, j))
            for neighbor in vicinity:
                if neighbor not in self.get_to_explore():
                    self.__to_explore.append(neighbor)
            return status, percepts, cost

        # Sinon, on prend la première position qu'il faut explorer
        (i, j) = self.__to_explore.pop(0)
        # On l'inspecte
        status = self.inspect_pos(i, j)
        # Si elle n'est pas sûre on laisse tomber pour le moment
        if status == "uncertain":
            return "[OK]", "Aucune position explorée", 0
        # Sinon on envoie la bonne sonde
        if status == "safe":
            status, percepts, cost = self.explore_safe_pos(i, j)
        elif status == "dangerous":
            return "[OK]", "Dangerosité déduite", 0
        else:
            return "[Err]", "Erreur", 0
        # Et on ajoute les voisins pas encore explorés à la liste de tâches
        vicinity = self.get_uncertain_neighbors((i, j))
        for neighbor in vicinity:
            if neighbor not in self.get_to_explore():
                self.__to_explore.append(neighbor)
        return status, percepts, cost

    def explore_safe_pos(self, i: int, j: int) -> Tuple[str, str, str]:
        if self.verbose:
            print(f"La case ({i}, {j}) a l'air sûre, envoyons une sonde.")
        status, percepts, cost = self.wwr.probe(i, j)
        self.__add_knowledge(i, j, percepts)
        return(status, percepts, cost)

    def explore_uncertain_pos(self, i: int, j: int) -> Tuple[str, str, str]:
        if self.verbose:
            print(f"Impossible de déduire ce qu'il y a sur la case ({i}, {j}),\
 mieux vaut envoyer une sonde précautionneuse.")
        status, percepts, cost = self.wwr.cautious_probe(i, j)
        self.__add_knowledge(i, j, percepts)
        return(status, percepts, cost)

    # Ranger les cibles dans un ordre à peu près intelligent
    def order_targets(self):
        # Cas particulier où il y a seulement de l'or en (0, 0)
        if self.get_targets() == [(0, 0)]:
            return

        self.__targets.append((0, 0))
        self.__targets = gen_opt_path(self.get_safe_pos(), self.get_targets(),
                                      self.verbose)
        while self.get_targets()[-1] != (0, 0):
            shift(self.__targets)
        if self.verbose:
            print(f"Les cases à visiter sont : {self.get_targets()}")
        return

    # Aller à la première cible depuis la position actuelle
    def go_to_target(self, i: int, j: int) -> Tuple:
        # S'il n'y a plus de cible, on peut déclarer la phase 2 finie
        if len(self.get_targets()) == 0:
            return "[OK]", "Il n'y a plus aucune cible à atteindre", 0

        # Sinon, on prend la première dans la liste
        target = self.__targets.pop(0)
        # Et on calcule le chemin jusque là
        path = astar(self.get_safe_pos(), (i, j), target)

        # S'il n'y a pas de chemin
        if path is None:
            return "[OK]", "Il n'y a pas de chemin vers cette cible", 0

        if self.verbose:
            print(f"Pour aller de ({i}, {j}) à {target}, le chemin \
{path[::-1]} a été trouvé")

        # Cas particlier de l'or en (0, 0)
        if len(path) == 0:
            status, msg, cost = self.wwr.go_to(i, j)
            if self.verbose:
                print(msg)
            return status, msg, cost

        # Sinon, on y va étape par étape
        total_cost = 0
        while len(path) != 0:
            ni, nj = path.pop()
            status, msg, cost = self.wwr.go_to(ni, nj)
            total_cost += cost
            if self.verbose:
                print(msg)
            if status == "[KO]":
                return status, msg, total_cost

        return "[OK]", f"Vous êtes bien arrivé en {target}", total_cost
