# Chassons le Wumpus !

Repo du binôme 28 (Nelle Fuseau et Bertille Dubosc de Pesquidoux)

Les méthodes qui font l'interface entre le solveur SAT et le client du Wumpus sont déclarées dans lib/solver.py. Elles sont regroupées dans une classe SolvableWorldRemote.

Ce fichier contient aussi la stratégie d'exploration du monde dans la fonction explore(), un appel à un algorithme pour ordonner les cibles à visiter qui est écrit dans travelingsalesperson.py, et un appel à un algorithme A* développé dans pathfinder.py.

C'est dans main.py que sont appelées la fonction d'exploration et de déplacement autant de fois que nécessaire.

## Dépendances

`progressbar`, à installer avec pip.
